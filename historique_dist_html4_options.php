<?php
/**
 * Options au chargement du plugin Dist HTML4
 *
 * @plugin     Squelette-dist en HTML 4
 * @copyright  2021
 * @author     collectif SPIP
 * @licence    GNU/GPL
 * @package    SPIP\Historique_dist_html4\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Limiter au HTML4 sur le site public
if (!defined('_VERSION_HTML')) {
	define ('_VERSION_HTML', 'html4');
}
