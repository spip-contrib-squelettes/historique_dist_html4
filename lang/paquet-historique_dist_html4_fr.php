<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'historique_dist_html4_description' => 'Plugin de compatibilité : les squelettes-dist avec un DOCTYPE HTML4 (rend inutile le plugin historique_spip32_html4)',
	'historique_dist_html4_nom' => 'Squelette-dist en HTML 4',
	'historique_dist_html4_slogan' => '',
);
